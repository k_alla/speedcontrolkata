/*
 * Copyright 2015-2018 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */

package com.example.project;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class FineCalculatorTest {



	@Test
	@DisplayName("10kmh too fast")
	void calculateWith10kmhTooFast() {
		FineCalculator calculator = new FineCalculator();
		assertEquals(10, calculator.getFine(10), "Fine for 10 km/h should be 10 EUROS");
	}

	
	@Test
	@DisplayName("0kmh too fast")
	void calculateWithNokmhTooFast() {
		FineCalculator calculator = new FineCalculator();
		assertEquals(0, calculator.getFine(0), "No Fine");
	}

	@Test
	@DisplayName("11kmh too fast")
	void calculateWithN11kmhTooFast() {
		FineCalculator calculator = new FineCalculator();
		assertEquals(20, calculator.getFine(11), "Fine for 11 km/h should be 10 EUROS");
	}

	@Test
	@DisplayName("15kmh too fast")
	void calculateWithN15kmhTooFast() {
		FineCalculator calculator = new FineCalculator();
		assertEquals(20, calculator.getFine(15), "Fine for 15 km/h should be 10 EUROS");
	}

	@Test
	@DisplayName("16kmh too fast")
	void calculateWithN16kmhTooFast() {
		FineCalculator calculator = new FineCalculator();
		assertEquals(30, calculator.getFine(16), "Fine for 16 km/h should be 30 EUROS");
	}

	@Test
	@DisplayName("20kmh too fast")
	void calculateWithN20kmhTooFast() {
		FineCalculator calculator = new FineCalculator();
		assertEquals(30, calculator.getFine(20), "Fine for 20 km/h should be 30 EUROS");
	}

	@Test
	@DisplayName("21kmh too fast")
	void calculateWithN21kmhTooFast() {
		FineCalculator calculator = new FineCalculator();
		assertEquals(70, calculator.getFine(21), "Fine for 21 km/h should be 70 EUROS");
	}

	@Test
	@DisplayName("25kmh too fast")
	void calculateWithN25kmhTooFast() {
		FineCalculator calculator = new FineCalculator();
		assertEquals(70, calculator.getFine(25), "Fine for 25 km/h should be 70 EUROS");
	}

	@Test
	@DisplayName("26kmh too fast")
	void calculateWithN26kmhTooFast() {
		FineCalculator calculator = new FineCalculator();
		assertEquals(80, calculator.getFine(26), "Fine for 26 km/h should be 80 EUROS");
	}

	@Test
	@DisplayName("30kmh too fast")
	void calculateWithN30kmhTooFast() {
		FineCalculator calculator = new FineCalculator();
		assertEquals(80, calculator.getFine(30), "Fine for 30 km/h should be 80 EUROS");
	}
}
